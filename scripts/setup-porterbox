#!/bin/bash
set -e
set -u
set -o pipefail

if [ ! -f "$0" ]; then
	# Can't use die yet - setup-common needs to be sourced
	echo >&2 "$0: Cannot be run from PATH"
	exit 1
fi
puppet_root=`readlink -f "\`dirname "$0"\`/../"`

. "$puppet_root"/scripts/setup-common

# +====================================================================+
# | Determine host details                                             |
# +====================================================================+

debian_ports=yes
debian_unreleased=yes
debian_mirror="http://deb.debian.org/debian"
debian_ports_mirror=
debian_security=
apt_http_proxy=
apt_ftp_proxy=
enable_notify_monitor=yes
public_porterbox=no
case "$hostname" in
	andi|landau|nvg5120|paq|pad|paco|pacific|paladin|phantom|mx3210|atlas|raverin|ravirin|sibaris)
		die "$hostname is not a porterbox"
		;;

	# +----------------------------------------------------------------+
	# | hppa/parisc                                                    |
	# +----------------------------------------------------------------+

	panama)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		apt_http_proxy="http://localhost:3142"
		public_porterbox=yes
		;;
	parisc)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		apt_http_proxy="http://localhost:3142"
		public_porterbox=yes
		;;

	# +----------------------------------------------------------------+
	# | ia64                                                           |
	# +----------------------------------------------------------------+

	yttrium)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;

	# +----------------------------------------------------------------+
	# | kfreebsd                                                       |
	# +----------------------------------------------------------------+

	lemon)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;

	# +----------------------------------------------------------------+
	# | loong64                                                        |
	# +----------------------------------------------------------------+

	shenzhou)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;

	# +----------------------------------------------------------------+
	# | m68k                                                           |
	# +----------------------------------------------------------------+

	mitchy)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;

	# +----------------------------------------------------------------+
	# | ppc64                                                          |
	# +----------------------------------------------------------------+

	perotto)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;
	redpanda)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		;;

	# +----------------------------------------------------------------+
	# | sparc64                                                        |
	# +----------------------------------------------------------------+

	deb4g)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		apt_http_proxy="http://localhost:3128"
		;;
	kyoto)
		debian_ports_mirror="http://ftp.ports.debian.org/debian-ports"
		public_porterbox=yes
		;;
	notker|sakharov)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;
	stadler)
		debian_ports_mirror="http://deb.debian.org/debian-ports"
		public_porterbox=yes
		;;
esac

# +====================================================================+
# | Setup schroot                                                      |
# +====================================================================+

ensure_file "/etc/schroot/setup.d/99porterbox-extra-apt-options" - root root 555 < "$puppet_root/modules/schroot/files/schroot-setup.d/99porterbox-extra-apt-options"
ensure_file "/etc/schroot/setup.d/99porterbox-extra-sources" - root root 555 < "$puppet_root/modules/schroot/files/schroot-setup.d/99porterbox-extra-sources"

case "$arch" in
	kfreebsd-*)
		ensure_file "/etc/schroot/setup.d/99shm" - root root 555 < "$puppet_root/modules/schroot/files/schroot-setup.d/99shm"
	;;
esac

ensure_file "/usr/local/sbin/setup-dchroot" - root root 555 < "$puppet_root/modules/schroot/files/setup-dchroot"
ensure_file "/usr/local/sbin/setup-all-dchroots" - root root 555 < "$puppet_root/modules/schroot/files/setup-all-dchroots"

if [ "$public_porterbox" != 'no' ]; then
	if true; then
		cat <<EOF
##
### THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
### USE: git clone git+ssh://\$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
###

deb     https://db.debian.org/debian-admin       debian-all      main
#deb     https://db.debian.org/debian-admin       stretch main
EOF
	fi | ensure_file "/etc/apt/sources.list.d/db.debian.org.list" - root root 444

	ensure_file "/etc/apt/trusted.gpg.d/db.debian.org.gpg" - root root 444 < "$puppet_root/modules/debian-org/files/db.debian.org.gpg"

	ensure_directory "/etc/ssh/userkeys" 755 root root

	if [ ! -e /etc/ssh/userkeys/root -a ! -e /var/lib/misc/userkeys/root -a ! -e /etc/ssh/userkeys/root.more ]; then
		if [ ! -e ~root/.ssh/authorized_keys ]; then
			die "No ~root/.ssh/authorized_keys to copy to /etc/ssh/userkeys/root"
		fi
		run cp ~root/.ssh/authorized_keys /etc/ssh/userkeys/root
		run touch ~root/.ssh/authorized_keys-MOVED-TO-etc_ssh_userkeys
	fi

	if true; then
		cat << EOF
##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://\$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# Package generated configuration file
# See the sshd(8) manpage for details

# What ports, IPs and protocols we listen for
Port 22
# Use these options to restrict which interfaces/protocols sshd will bind to
#ListenAddress ::
#ListenAddress 0.0.0.0
Protocol 2
# HostKeys for protocol version 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
#Privilege Separation is turned on for security
UsePrivilegeSeparation yes

# Logging
SyslogFacility AUTH
LogLevel INFO

# Authentication:
LoginGraceTime 120
PermitRootLogin without-password
StrictModes yes

PubkeyAuthentication yes

# Don't read the user's ~/.rhosts and ~/.shosts files
IgnoreRhosts yes
# For this to work you will also need host keys in /etc/ssh_known_hosts
HostbasedAuthentication no
# Uncomment if you don't trust ~/.ssh/known_hosts for RhostsRSAAuthentication
#IgnoreUserKnownHosts yes

# To enable empty passwords, change to yes (NOT RECOMMENDED)
PermitEmptyPasswords no

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosGetAFSToken no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes

# GSSAPI options
#GSSAPIAuthentication no
#GSSAPICleanupCredentials yes

X11Forwarding no
X11DisplayOffset 10
PrintMotd no
PrintLastLog yes
TCPKeepAlive yes
#UseLogin no

#MaxStartups 10:30:60
#Banner /etc/issue.net

# Allow client to pass locale environment variables
AcceptEnv LANG LC_*

Subsystem sftp /usr/lib/openssh/sftp-server

UsePAM yes

AuthorizedKeysFile /etc/ssh/userkeys/%u /var/lib/misc/userkeys/%u /etc/ssh/userkeys/%u.more

PasswordAuthentication no

Match Group sftponly
  AllowStreamLocalForwarding no
  AllowTCPForwarding no
  X11Forwarding no
  ForceCommand internal-sftp
EOF
	fi | ensure_file "/etc/ssh/sshd_config" - root root 644

	ensure_file "/etc/nsswitch.conf" - root root 644 < "$puppet_root/modules/debian-org/files/nsswitch.conf"

	if [ -f "/lib/`dpkg-architecture -qDEB_HOST_MULTIARCH`/security/pam_pwdfile.so" ]; then
		ensure_file "/etc/pam.d/sudo" - root root 444 < "$puppet_root/modules/sudo/files/pam"
	else
		die "Missing package: libpam-pwdfile. Please install to continue."
	fi

	if true; then
		cat <<EOF
##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://\$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

#
# /etc/pam.d/common-session - session-related modules common to all services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of sessions of *any* kind (both interactive and
# non-interactive).
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session [default=1]                     pam_permit.so
# here's the fallback if no module succeeds
session requisite                       pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session required                        pam_permit.so
# and here are more per-package modules (the "Additional" block)
session required        pam_unix.so
# end of pam-auth-update config
session [success=1 default=ignore]      pam_succeed_if.so quiet_fail quiet_success home = /nonexistent
session optional                        pam_mkhomedir.so skel=/etc/skel umask=0022
session optional                        pam_systemd.so
session optional                        pam_permit.so	cat <<EOF
EOF
	fi | ensure_file "/etc/pam.d/common-session" - root root 444

	if true; then
		cat <<EOF
##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://\$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

#
# /etc/pam.d/common-session-noninteractive - session-related modules
# common to all non-interactive services
#
# This file is included from other service-specific PAM config files,
# and should contain a list of modules that define tasks to be performed
# at the start and end of all non-interactive sessions.
#
# As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
# To take advantage of this, it is recommended that you configure any
# local modules either before or after the default block, and use
# pam-auth-update to manage selection of other modules.  See
# pam-auth-update(8) for details.

# here are the per-package modules (the "Primary" block)
session [default=1]                     pam_permit.so
# here's the fallback if no module succeeds
session requisite                       pam_deny.so
# prime the stack with a positive return value if there isn't one already;
# this avoids us returning an error just because nothing sets a success code
# since the modules above will each just jump around
session required                        pam_permit.so
# and here are more per-package modules (the "Additional" block)
session required        pam_unix.so
# end of pam-auth-update config
EOF
	fi | ensure_file "/etc/pam.d/common-session-noninteractive" - root root 444

	if true; then
		# ud-replicated only works from machines under DSA control, so we have to
		# override ud-replicate to be more frequent than weekly.
		cat <<EOF
# crontab for ud-replicate
MAILTO=$admin_mail
# min        hour         day mon wday user     cmd
*/15         *            *   *   *    root     if [ -x /usr/bin/ud-replicate ]; then /usr/bin/ud-replicate; fi
EOF
	fi | ensure_file "/etc/cron.d/ud-replicate" - root root
fi

if true; then
	if [ "$public_porterbox" = 'no' ]; then
		cat <<EOF
%Debian,%guest,%d-i,%sbuild ALL=(ALL) NOPASSWD: /usr/local/bin/dd-schroot-cmd
EOF
	else
		cat <<EOF
%Debian,%guest,%d-i ALL=(ALL) NOPASSWD: /usr/local/bin/dd-schroot-cmd
EOF
	fi
fi | ensure_file "/etc/sudoers.d/dd-schroot-cmd" - root root 440

ensure_directory "/etc/schroot/dsa" 755 root root
# No custom default mirror for now (ports mirror for binaries is set in
# /etc/schroot/setup.d/99porterbox* and /usr/local/sbin/setup-all-dchroots)
#ensure_file "/etc/schroot/dsa/default-mirror" "http://ftp.debian.org/debian" root root
ensure_file "/etc/schroot/dsa/config" - root root < "$puppet_root/modules/schroot/files/schroot-dsa/config"
if true; then
	# lifted from modules/schroot/templates/schroot-dsa/fstab.erb and converted
	# to shell
	cat <<EOF
# fstab: static file system information for chroots.
# Note that the mount point will be prefixed by the chroot path
# (CHROOT_PATH)
#
# <file system>	<mount point>	<type>	<options>	<dump>	<pass>

EOF
	case "$arch" in
		kfreebsd-*)
			cat <<EOF
# kFreeBSD version
proc		/proc		linprocfs	defaults	0	0
sys		/sys		linsysfs	defaults	0	0
dev		/dev		devfs		rw		0	0
dev		/dev/fd		fdescfs		rw		0	0
/home		/home		nullfs		rw		0	0
/tmp		/tmp		nullfs		rw		0	0
tmpfs-shm	/run/shm	tmpfs		nosuid,noexec,size=64m,mode=1777	0 0

EOF
			;;
		*)
			cat <<EOF
# Linux version
/proc		/proc		none	rw,bind		0	0
/sys		/sys		none	rw,bind		0	0
/dev		/dev		none	rw,bind		0	0
EOF
			# TODO: Make unconditional once updated everywhere
			if dpkg --compare-versions "`dpkg-query -W -f '${Version}' schroot`" ge '1.6.13-4~'; then
				cat <<EOF
/dev/pts	/dev/pts	devpts	rw,newinstance,ptmxmode=666,mode=620,gid=5	0 0
EOF
			else
				cat <<EOF
/dev/pts	/dev/pts	none	rw,bind		0	0
EOF
			fi
			cat <<EOF
/home		/home		none	rw,bind		0	0
/tmp		/tmp		none	rw,bind		0	0
tmpfs-shm	/dev/shm	tmpfs	defaults,size=64m	0 0

EOF
			;;
	esac
fi | ensure_file "/etc/schroot/dsa/fstab" - root root

if true; then
	cat <<EOF
debian_ports='${debian_ports}'
debian_unreleased='${debian_unreleased}'
debian_mirror='${debian_mirror}'
EOF
	if [ -n "${debian_ports_mirror}" ]; then
		cat <<EOF
debian_ports_mirror='${debian_ports_mirror}'
EOF
	fi
	if [ -n "${debian_security}" ]; then
		cat <<EOF
debian_security='${debian_security}'
EOF
fi
	if [ -n "${apt_http_proxy}" ]; then
		cat <<EOF
apt_http_proxy='${apt_http_proxy}'
EOF
	fi
	if [ -n "${apt_ftp_proxy}" ]; then
		cat <<EOF
apt_ftp_proxy='${apt_ftp_proxy}'
EOF
	fi
fi | ensure_file "/etc/schroot/conf.dsa" - root root

ensure_file "/usr/local/bin/dd-schroot-cmd" - root root 555 < "$puppet_root/modules/porterbox/files/dd-schroot-cmd"
ensure_file "/usr/local/bin/schroot-list-sessions" - root root 555 < "$puppet_root/modules/porterbox/files/schroot-list-sessions"

# +====================================================================+
# | Setup cron                                                         |
# +====================================================================+

if true; then
	cat <<EOF
# crontab for dsa-puppet
MAILTO=$admin_mail
# min        hour         day mon wday user     cmd
0            *            *   *   *    root     cd "$puppet_root" && chronic git fetch && chronic git reset --hard origin/master && cd ~root && "$puppet_root/scripts/setup-porterbox"
EOF
	if [ "$enable_notify_monitor" != 'no' ]; then
		cat <<EOF
*/5          *            *   *   *    root     "$puppet_root/scripts/notify-monitor"
EOF
	fi
fi | ensure_file /etc/cron.d/dsa-puppet - root root

if true; then
	cat <<EOF
# crontab for porterbox
MAILTO=$admin_mail
# min        hour         day mon wday user     cmd
0            15           *   *   0    root     PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots porterbox
*/5          *            *   *   *    root     if [ -f ~root/SETUP-DCHROOTS-PLEASE ]; then rm ~root/SETUP-DCHROOTS-PLEASE && PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots porterbox; fi
EOF
fi | ensure_file /etc/cron.d/puppet-update-dchroots - root root
