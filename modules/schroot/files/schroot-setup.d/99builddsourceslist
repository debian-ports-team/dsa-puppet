#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

# vim:set et ts=4 sw=4:
# Copyright © 2010 Marc Brockschmidt <he@debian.org>
# Copyright © 2010 Andreas Barth <aba@not.so.argh.org>
# Copyright © 2010 Philipp Kern <pkern@debian.org>
#
# buildd is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# buildd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#
#####################################################################

set -e
shopt -s extglob

[ "$CHROOT_PROFILE" = "buildd" ] || exit 0

. "$SETUP_DATA_DIR/common-data"
. "$SETUP_DATA_DIR/common-functions"
. "$SETUP_DATA_DIR/common-config"

# The test below tries to determine if a source chroot is being used.
# The apt configuration is generated only for non-source chroots.
#
# It originally tested CHROOT_SESSION_PURGE, but while it works for
# LVM based chroots, it doesn't for tar based chroots which always
# needed to be purged.
#
# Future versions of schroot will implement CHROOT_SESSION_SOURCE for
# a more robust way to check for source chroots.
#
# See bug#718127 for more details.
if ! echo "$CHROOT_DESCRIPTION" | grep -q '(source chroot)' ; then
    CHROOT_ALIAS="${CHROOT_ALIAS/#experimental-/sid-experimental-}"
    case $CHROOT_ALIAS in
        +([^-])-kfreebsd-+([^-])-kfreebsd-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1-2 -d-)
        SUITE_VARIANT=$(echo $CHROOT_ALIAS | cut -f3 -d-)
        ;;
        +([^-])-kfreebsd-kfreebsd-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1-2 -d-)
        if [ ${SUITE_BASE} != "sid" ]; then
            SUITE_VARIANT="proposed-updates"
        fi
        ;;
        +([^-])-+([^-])-@(kfreebsd|hurd)-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1 -d-)
        SUITE_VARIANT=$(echo $CHROOT_ALIAS | cut -f2 -d-)
        ;;
        *-*-*-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1 -d-)
        SUITE_VARIANT=$(echo $CHROOT_ALIAS | cut -f2,3 -d-)
        ;;
        +([^-])-@(kfreebsd|hurd)-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1 -d-)
        if [ ${SUITE_BASE} != "sid" ]; then
            SUITE_VARIANT="proposed-updates"
        fi
        ;;
        *-*-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1 -d-)
        SUITE_VARIANT=$(echo $CHROOT_ALIAS | cut -f2 -d-)
        ;;
        *-*-sbuild*)
        SUITE_BASE=$(echo $CHROOT_ALIAS | cut -f1 -d-)
        if [ ${SUITE_BASE} != "sid" ]; then
            SUITE_VARIANT="proposed-updates"
        fi
        ;;
    esac
fi

VERBOSE=""
if [ "$AUTH_VERBOSITY" = "verbose" ]; then
      VERBOSE="--verbose"
fi

if [ -f /etc/schroot/dsa/default-mirror ]; then
    debian_mirror=$(cat /etc/schroot/dsa/default-mirror)
fi
if [ -f /etc/schroot/conf.buildd ]; then
    . /etc/schroot/conf.buildd
fi

if [ -z "${debian_ports}" ]; then
    debian_ports=no
fi

if [ -z "${debian_unreleased}" ]; then
    debian_unreleased=no
fi

function domirror() {
    if [ $# -eq 2 ]; then
        if echo $1 | grep -q 'SUITE'; then
            for SUITE in ${SUITES:-SUITE}; do
                echo deb $1     | sed -e "s,COMPONENT,${COMPONENT},"     -e "s,SUITE,${SUITE}," >> $2
                echo deb-src $1 | sed -e "s,COMPONENT,${COMPONENT_SRC}," -e "s,SUITE,${SUITE}," >> $2
            done
        else
            echo deb $1     | sed -e "s,COMPONENT,${COMPONENT},"     >> $2
            echo deb-src $1 | sed -e "s,COMPONENT,${COMPONENT_SRC}," >> $2
        fi
    elif [ $# -eq 3 ]; then
        if echo $1 | grep -q 'SUITE'; then
            for SUITE in ${SUITES:-SUITE}; do
                echo deb $1     | sed -e "s,COMPONENT,${COMPONENT},"     -e "s,SUITE,${SUITE}," >> $3
                echo deb-src $2 | sed -e "s,COMPONENT,${COMPONENT_SRC}," -e "s,SUITE,${SUITE}," >> $3
            done
        else
            echo deb $1     | sed -e "s,COMPONENT,${COMPONENT},"     >> $3
            echo deb-src $2 | sed -e "s,COMPONENT,${COMPONENT_SRC}," >> $3
        fi
    else
        echo "ERROR: Bad number of arguments ($#) to domirror - exiting"
        exit 1
    fi
}

if [ "$1" = "setup-start" ] || [ "$1" = "setup-recover" ]; then
    if [ -n "${SUITE_BASE}" ] && ( [ -f /etc/schroot/conf.buildd ] || [ -f /etc/schroot/dsa/default-mirror ] ); then
        APT_LIST="${CHROOT_PATH}/etc/apt/sources.buildd.list"
        APT_CONF="${CHROOT_PATH}/etc/apt/apt.conf.d/99buildd.conf"
        APT_KEYS=""
        rm -f "${APT_LIST}" "${APT_CONF}"

        if [ "${debian_ports}" != 'no' ]; then
            case "${SUITE_BASE}" in
                sid)
                    ;;
                *)
                    echo "ERROR: Bad SUITE_BASE ($SUITES_BASE) for ports architecture - exiting"
                    exit 1
                    ;;
            esac
            COMPONENT="main"
            COMPONENT_SRC="main"
        else
            COMPONENT="main contrib"
            COMPONENT_SRC="main contrib non-free"
        fi

        if [ "${SUITE_VARIANT}" = 'proposed-updates' ]; then
            SUITES="${SUITE_BASE} ${SUITE_BASE}-proposed-updates"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" ${APT_LIST}
            domirror "http://ftp.debian.org/debian SUITE COMPONENT" ${APT_LIST}
            if [ "${debian_incoming}" != 'no' ]; then
                SUITES="${SUITE_BASE}-proposed-updates"
                domirror "http://incoming.debian.org/debian-buildd buildd-SUITE COMPONENT" ${APT_LIST}
            fi

        elif [ "${SUITE_VARIANT}" = 'lts' ]; then
            SUITES="${SUITE_BASE} ${SUITE_BASE}-lts"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" ${APT_LIST}
            domirror "http://ftp.debian.org/debian SUITE COMPONENT" ${APT_LIST}
            if [ "${debian_incoming}" != 'no' ]; then
                SUITES="${SUITE_BASE}-lts"
                domirror "http://incoming.debian.org/debian-buildd buildd-SUITE COMPONENT" ${APT_LIST}
            fi

        elif [ "${SUITE_VARIANT}" = 'security' ]; then
            SUITES="${SUITE_BASE}"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} ${SUITE_BASE} COMPONENT" ${APT_LIST}
            domirror "http://ftp.debian.org/debian ${SUITE_BASE} COMPONENT" ${APT_LIST}
            [ -n "${security_mirror}" ] && domirror "${security_mirror} ${SUITE_BASE}/updates COMPONENT" ${APT_LIST}
            domirror "http://security-master.debian.org/debian-security ${SUITE_BASE}/updates COMPONENT" ${APT_LIST}
            domirror "http://security-master.debian.org/debian-security-buildd buildd-${SUITE_BASE}/updates COMPONENT" ${APT_LIST}

        elif [ "${SUITE_VARIANT%%-sloppy}" = 'backports' ]; then
            SUITES="${SUITE_BASE}"
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" ${APT_LIST}
            domirror "http://ftp.debian.org/debian SUITE COMPONENT" ${APT_LIST}
            [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE-backports COMPONENT" ${APT_LIST}
            domirror "http://ftp.debian.org/debian SUITE-backports COMPONENT" ${APT_LIST}
            if [ "${debian_incoming}" != 'no' ]; then
                domirror "http://incoming.debian.org/debian-buildd buildd-SUITE-backports COMPONENT" ${APT_LIST}
            fi
            if [ "$(echo ${SUITE_VARIANT} | cut -d - -f 2)" = "sloppy" ]; then
                [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE-backports-sloppy COMPONENT" ${APT_LIST}
                domirror "http://ftp.debian.org/debian SUITE-backports-sloppy COMPONENT" ${APT_LIST}
                if [ "${debian_incoming}" != 'no' ]; then
                    domirror "http://incoming.debian.org/debian-buildd buildd-SUITE-backports-sloppy COMPONENT" ${APT_LIST}
                fi
            fi

        elif [ "${SUITE_BASE}" = 'sid' ]; then
            SUITES="unstable"
            if [ "${SUITE_VARIANT}" = "experimental" ]; then
                SUITES="unstable experimental"
            fi
            if [ "${debian_ports}" != 'no' ]; then
                # Need different deb and deb-src lines, but only one mirror can be given
                # For now, disallow this
                if [ -n "${debian_mirror}" ]; then
                    echo "ERROR: debian_mirror (${debian_mirror}) given for ports architecture - exiting"
                    exit 1
                fi
                domirror "http://ftp.ports.debian.org/debian-ports SUITE COMPONENT" "http://ftp.debian.org/debian SUITE COMPONENT" ${APT_LIST}
            else
                [ -n "${debian_mirror}" ] && domirror "${debian_mirror} SUITE COMPONENT" ${APT_LIST}
                domirror "http://ftp.debian.org/debian SUITE COMPONENT" ${APT_LIST}
            fi
            if [ "${debian_unreleased}" != 'no' ]; then
                if [ "${debian_ports}" != 'no' ]; then
                    domirror "http://ftp.ports.debian.org/debian-ports unreleased COMPONENT" "http://ftp.ports.debian.org/debian-ports unreleased COMPONENT" ${APT_LIST}
                else
                    echo "ERROR: only ports has an unreleased suite - exiting"
                    exit 1
                fi
            fi
            if [ "${debian_incoming}" != 'no' ]; then
                if [ "${debian_ports}" != 'no' ]; then
                    domirror "http://incoming.ports.debian.org/buildd SUITE COMPONENT" "http://incoming.debian.org/debian-buildd buildd-SUITE COMPONENT" ${APT_LIST}
                else
                    domirror "http://incoming.debian.org/debian-buildd buildd-SUITE COMPONENT" ${APT_LIST}
                fi
            fi

        else
            echo "ERROR: cannot adjust sources.list: ${SUITE_BASE}/${SUITE_VARIANT} unknown - exiting"
            exit 1

        fi
        echo 'Acquire::PDiffs "false";' >> ${APT_CONF}
        echo 'APT::Install-Recommends 0;' >> ${APT_CONF}
        echo 'Dir::Etc::SourceList "sources.buildd.list";' >> ${APT_CONF}
        echo 'Acquire::Languages "none";' >> ${APT_CONF}
        [ -n "${apt_http_proxy}" ] && echo "Acquire::http::proxy \"${apt_http_proxy}\";" >> ${APT_CONF}
        [ -n "${apt_ftp_proxy}" ] && echo "Acquire::ftp::proxy \"${apt_ftp_proxy}\";" >> ${APT_CONF}

        if [ -n "${APT_KEYS}" ]; then
            for KEY in ${APT_KEYS}; do
                if [ -f /usr/share/buildd/${KEY}.asc ]; then
                    chroot ${CHROOT_PATH} apt-key add - < /usr/share/buildd/${KEY}.asc >&2
                else
                    echo W: should add apt key ${KEY} but not found >&2
                fi
            done
        fi
    else
    if [ -n "$SUITE_VARIANT" ]; then
        SOURCES_FILE_NAME="${SUITE_BASE}-${SUITE_VARIANT}.sources"
        SOURCES_FILE_PATH="/etc/schroot/buildd.d/${SOURCES_FILE_NAME}"
                APT_LIST_AUTO="${CHROOT_PATH}/etc/apt/sources.list.d/auto.list"

        if [ -f "$SOURCES_FILE_PATH" ]; then
                cp $VERBOSE ${SOURCES_FILE_PATH} "${CHROOT_PATH}/etc/apt/sources.list.d/${SOURCES_FILE_NAME}.list"
                elif [ ${SUITE_VARIANT} = 'security' ]; then
                        : > ${APT_LIST_AUTO}
                    if [ -n "${security_mirror}" ]; then
                        echo deb ${security_mirror} ${SUITE_BASE}/updates main contrib >> ${APT_LIST_AUTO}
                        echo deb-src ${security_mirror} ${SUITE_BASE}/updates main contrib >> ${APT_LIST_AUTO}
                    fi
                    echo deb http://security-master.debian.org/debian-security ${SUITE_BASE}/updates main contrib >> ${APT_LIST_AUTO}
                    echo deb-src http://security-master.debian.org/debian-security ${SUITE_BASE}/updates main contrib >> ${APT_LIST_AUTO}
                    echo deb http://security-master.debian.org/debian-security-buildd buildd-${SUITE_BASE}/updates main contrib >> ${APT_LIST_AUTO}
                    echo deb-src http://security-master.debian.org/debian-security-buildd buildd-${SUITE_BASE}/updates main contrib >> ${APT_LIST_AUTO}
                elif [ ${SUITE_VARIANT%%-sloppy} = 'backports' ]; then
                        : > ${APT_LIST_AUTO}
                    if [ -n "${debian_mirror}" ]; then
                        echo deb ${debian_mirror} ${SUITE_BASE}-backports main contrib >> ${APT_LIST_AUTO}
                        echo deb-src ${debian_mirror} ${SUITE_BASE}-backports main contrib non-free >> ${APT_LIST_AUTO}
                    fi
                    echo deb http://ftp.debian.org/debian ${SUITE_BASE}-backports main contrib >> ${APT_LIST_AUTO}
                    echo deb-src http://ftp.debian.org/debian ${SUITE_BASE}-backports main contrib non-free >> ${APT_LIST_AUTO}
                    echo deb http://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports main contrib  >> ${APT_LIST_AUTO}
                    echo deb-src http://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports main contrib non-free >> ${APT_LIST_AUTO}
                    if [ "$(echo ${SUITE_VARIANT} | cut -d - -f 2)" = "sloppy" ]; then
                        if [ -n "${debian_mirror}" ]; then
                            echo deb ${debian_mirror} ${SUITE_BASE}-backports-sloppy main contrib >> ${APT_LIST_AUTO}
                            echo deb-src ${debian_mirror} ${SUITE_BASE}-backports-sloppy main contrib non-free >> ${APT_LIST_AUTO}
                        fi
                        echo deb http://ftp.debian.org/debian ${SUITE_BASE}-backports-sloppy main contrib >> ${APT_LIST_AUTO}
                        echo deb-src http://ftp.debian.org/debian ${SUITE_BASE}-backports-sloppy main contrib non-free >> ${APT_LIST_AUTO}
                        echo deb http://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports-sloppy main contrib  >> ${APT_LIST_AUTO}
                        echo deb-src http://incoming.debian.org/debian-buildd buildd-${SUITE_BASE}-backports-sloppy main contrib non-free >> ${APT_LIST_AUTO}
                    fi
                elif [ ${SUITE_VARIANT} = 'experimental' ]; then
                    : > ${APT_LIST_AUTO}
                    if [ "${debian_ports}" != 'no' ]; then
                        # Need different deb and deb-src lines, but only one mirror can be given
                        # For now, disallow this
                        if [ -n "${debian_mirror}" ]; then
                            echo "ERROR: debian_mirror (${debian_mirror}) given for ports architecture - exiting"
                            exit 1
                        fi
                        echo deb     http://ftp.ports.debian.org/debian experimental main >> ${APT_LIST_AUTO}
                        echo deb-src http://ftp.debian.org/debian experimental main >> ${APT_LIST_AUTO}
                        echo deb     http://incoming.ports.debian.org/buildd experimental main >> ${APT_LIST_AUTO}
                        echo deb-src http://incoming.debian.org/debian-buildd buildd-experimental main >> ${APT_LIST_AUTO}
                    else
                        if [ -n "${debian_mirror}" ]; then
                            echo deb ${debian_mirror} experimental main contrib >> ${APT_LIST_AUTO}
                            echo deb-src ${debian_mirror} experimental main contrib non-free >> ${APT_LIST_AUTO}
                        fi
                        echo deb     http://ftp.debian.org/debian experimental main contrib >> ${APT_LIST_AUTO}
                        echo deb-src http://ftp.debian.org/debian experimental main contrib non-free >> ${APT_LIST_AUTO}
                        echo deb     http://incoming.debian.org/debian-buildd buildd-experimental main contrib >> ${APT_LIST_AUTO}
                        echo deb-src http://incoming.debian.org/debian-buildd buildd-experimental main contrib non-free >> ${APT_LIST_AUTO}
                    fi
        fi
    fi
    fi
elif [ "$1" = "setup-stop" ]; then
    if [ -n "${SUITE_BASE}" ] && ( [ -f /etc/schroot/conf.buildd ] || [ -f /etc/schroot/dsa/default-mirror ] ); then
        APT_LIST="${CHROOT_PATH}/etc/apt/sources.buildd.list"
        APT_CONF="${CHROOT_PATH}/etc/apt/apt.conf.d/99buildd.conf"
        rm -f "${APT_LIST}" "${APT_CONF}"
    else
    if [ -n "$SUITE_VARIANT" ]; then
        SOURCES_FILE_NAME="${SUITE_BASE}-${SUITE_VARIANT}.sources"
        SOURCES_FILE_PATH="/etc/schroot/buildd.d/${SOURCES_FILE_NAME}"

        if [ -f "$SOURCES_FILE_PATH" ]; then
                rm -f $VERBOSE "${CHROOT_PATH}/etc/apt/sources.list.d/${SOURCES_FILE_NAME}.list"
                elif [ ${SUITE_VARIANT} = 'security' ] || [ ${SUITE_VARIANT} = 'backports' ] || [ ${SUITE_VARIANT} = 'experimental' ] ; then
                rm -f $VERBOSE "${CHROOT_PATH}/etc/apt/sources.list.d/auto.list"
        fi
    fi
    fi
fi
